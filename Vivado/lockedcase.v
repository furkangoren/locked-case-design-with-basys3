`timescale 1ns / 1ps

// 												Module tanımlaması

module lockedcase(
clk,
JA,
clr,
JB,
PWM,
an,
seg
);


// 											Portların açıklanması

input clk;					// 100Mhz clock
input clr;                  // Motoru resetleme tuşu
inout [7:0] JB;             // Motor pinleri
inout [7:0] JA;			// Basys3'teki JB portu, JB[3:0] Sütun, JB[10:7] satır
output [3:0] an;			// 7-segment displaydeki anotlar
output [6:0] seg;	   // 7-segment displaydeki katotlar
output PWM;            //PWM çıkışı



// 							  		Parametre, Registerlar, and Wirelar


wire [3:0] an;
wire [6:0] seg;
wire [3:0] keypad;


//FSM parametreleri
reg [3:0] solvestate = 4'b0000;
reg [31:0] counter = 0;


//keypad
keypadc keypad_i0(
.clk(clk),
.Row(JA[7:4]),
.Col(JA[3:0]),
.keypadOut(keypad)
); //keypad'i çalıştırmak için

always @(posedge clk) begin//daha düşük sampling frekansta solvestate’i güncellemek için yavaş clock oluşturur
if (counter < 32'h007FFFFF)
begin
counter <= counter +1;
end
else if (counter == 32'h007FFFFF)//solvestate'i sadece counter tarafından tanımlanan özel aralıkta günceller
begin
counter <= 0;
//passcode arbitrarily set to: 71B8
if (solvestate == 4'b0000)//solvestate =0 durumunda ise 1. Duruma yollamak için "7"ye bas. Başka tuşa basılması durumunda solvestate=0'da kalacak.
begin
case (keypad)

4'h0 : solvestate <= 4'b0000; // 0
4'h1 : solvestate <= 4'b0000; // 1
4'h2 : solvestate <= 4'b0000; // 2
4'h3 : solvestate <= 4'b0000; // 3
4'h4 : solvestate <= 4'b0000; // 4
4'h5 : solvestate <= 4'b0000; // 5
4'h6 : solvestate <= 4'b0000; // 6
4'h7 : solvestate <= 4'b0001; // 7
4'h8 : solvestate <= 4'b0000; // 8
4'h9 : solvestate <= 4'b0000; // 9
4'hA : solvestate <= 4'b0000; // A
4'hB : solvestate <= 4'b0000; // B
4'hC : solvestate <= 4'b0000; // C
4'hD : solvestate <= 4'b0000; // D
4'hE : solvestate <= 4'b0000; // E
4'hF : solvestate <= 4'b0000; // F

default : solvestate <= 4'b0000;

endcase
end
else if (solvestate == 4'b0001)//Solvestate=1 ise 2.duruma yollamak için "1" butonuna bas
begin
case (keypad)

4'h0 : solvestate <= 4'b0000; // 0
4'h1 : solvestate <= 4'b0010; // 1
4'h2 : solvestate <= 4'b0000; // 2
4'h3 : solvestate <= 4'b0000; // 3
4'h4 : solvestate <= 4'b0000; // 4
4'h5 : solvestate <= 4'b0000; // 5
4'h6 : solvestate <= 4'b0000; // 6
4'h7 : solvestate <= 4'b0001; // 7
4'h8 : solvestate <= 4'b0000; // 8
4'h9 : solvestate <= 4'b0000; // 9
4'hA : solvestate <= 4'b0000; // A
4'hB : solvestate <= 4'b0000; // B
4'hC : solvestate <= 4'b0000; // C
4'hD : solvestate <= 4'b0000; // D
4'hE : solvestate <= 4'b0000; // E
4'hF : solvestate <= 4'b0000; // F

default : solvestate <= 4'b0001;

endcase
end
else if (solvestate == 4'b0010)//Solvestate 2.durumdayken 3.duruma yollamak için "B" butonuna bas.
begin
case (keypad)

4'h0 : solvestate <= 4'b0000; // 0
4'h1 : solvestate <= 4'b0010; // 1
4'h2 : solvestate <= 4'b0000; // 2
4'h3 : solvestate <= 4'b0000; // 3
4'h4 : solvestate <= 4'b0000; // 4
4'h5 : solvestate <= 4'b0000; // 5
4'h6 : solvestate <= 4'b0000; // 6
4'h7 : solvestate <= 4'b0000; // 7
4'h8 : solvestate <= 4'b0000; // 8
4'h9 : solvestate <= 4'b0000; // 9
4'hA : solvestate <= 4'b0000; // A
4'hB : solvestate <= 4'b0011; // B
4'hC : solvestate <= 4'b0000; // C
4'hD : solvestate <= 4'b0000; // D
4'hE : solvestate <= 4'b0000; // E
4'hF : solvestate <= 4'b0000; // F
default : solvestate <= 4'b0010;

endcase
end
else if (solvestate == 4'b0011)//solvestate = 3 ise 4.duruma göndermek için "8" butonuna basar.
begin
case (keypad)

4'h0 : solvestate <= 4'b0000; // 0
4'h1 : solvestate <= 4'b0000; // 1
4'h2 : solvestate <= 4'b0000; // 2
4'h3 : solvestate <= 4'b0000; // 3
4'h4 : solvestate <= 4'b0000; // 4
4'h5 : solvestate <= 4'b0000; // 5
4'h6 : solvestate <= 4'b0000; // 6
4'h7 : solvestate <= 4'b0000; // 7
4'h8 : solvestate <= 4'b0100; // 8
4'h9 : solvestate <= 4'b0000; // 9
4'hA : solvestate <= 4'b0000; // A
4'hB : solvestate <= 4'b0011; // B
4'hC : solvestate <= 4'b0000; // C
4'hD : solvestate <= 4'b0000; // D
4'hE : solvestate <= 4'b0000; // E
4'hF : solvestate <= 4'b0000; // F

default : solvestate <= 4'b0011;

endcase
end
else if (solvestate == 4'b0100)//Solvestate =4 durumunda "8"e basıldığı sürece aynı durumda kalır. Eğer başka butona basılırsa solvestate'i 0 durumuna yollar.
begin
case (keypad)

4'h0 : solvestate <= 4'b0000; // 0
4'h1 : solvestate <= 4'b0000; // 1
4'h2 : solvestate <= 4'b0000; // 2
4'h3 : solvestate <= 4'b0000; // 3
4'h4 : solvestate <= 4'b0000; // 4
4'h5 : solvestate <= 4'b0000; // 5
4'h6 : solvestate <= 4'b0000; // 6
4'h7 : solvestate <= 4'b0000; // 7
4'h8 : solvestate <= 4'b0100; // 8
4'h9 : solvestate <= 4'b0000; // 9
4'hA : solvestate <= 4'b0000; // A
4'hB : solvestate <= 4'b0000; // B
4'hC : solvestate <= 4'b0000; // C
4'hD : solvestate <= 4'b0000; // D
4'hE : solvestate <= 4'b0000; // E
4'hF : solvestate <= 4'b0000; // F

default : solvestate <= 4'b0100;

endcase
end
end
end




////servo motor

Servo_interface servo_i0(
.clr(clr),
.clk(clk),
.PWM(PWM)
    );

//-----------------------------------------------
//  		7 segment display kontrolü
//-----------------------------------------------

display display_i0(
.DispVal(solvestate),
.anode(an),
.segOut(seg)
);//solvestate'i ekranda göstermek için kodu çalıştırır

endmodule
